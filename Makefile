lint:
	cp flake8 ~/.config
	find . -name "*.py" | xargs python -m black
	find . -name "*.py" | xargs python -m isort
	find . -name "*.py" | xargs python -m flake8

mypy:
	find . -name "*.py" | xargs python -m mypy
